Odoo DevOps
=======================================

.. note:: The project is moved to https://itpp.dev/ops/ and will be shutdown here soon

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Docker/index
   Kubernetes/index
   Gitlab-ci/index
   git/index	
   ifttt/index
   lint
   remote-dev/index
